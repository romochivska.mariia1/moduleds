import scrapy
from bs4 import BeautifulSoup
from module.items import ProjectorItem

class ModuleproectorsSpider(scrapy.Spider):
    name = "moduleproectors"
    allowed_domains = ["hotline.ua"]
    start_urls = ["https://hotline.ua/ua/av/proektory/"]

    def parse(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')

        product_blocks = soup.find_all(class_='list-item__info')

        for block in product_blocks:
            title = block.find(class_='item-title text-md link link--black').text.strip()

            item = ProjectorItem()
            item['title'] = title

            specs_data = {}
            specs = block.find_all(class_='spec-item spec-item--bullet')
            for spec in specs:
                name = spec.text.strip()
                specs_data[name] = ""

            item['specs_data'] = specs_data

            yield item