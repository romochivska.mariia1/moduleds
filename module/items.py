import scrapy

class ProjectorItem(scrapy.Item):
    title = scrapy.Field()
    specs_data = scrapy.Field()
    